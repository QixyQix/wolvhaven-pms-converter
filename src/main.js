import './firebase'
import Vue from 'vue'
import App from './App.vue'
import VueFire from 'vuefire';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';

Vue.use(VueFire);
Vue.use(Buefy);


new Vue({
  el: '#app',
  render: h => h(App)
})
