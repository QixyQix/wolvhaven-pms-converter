import {initializeApp} from 'firebase'

const app = initializeApp({
    apiKey: "AIzaSyCqiZEkNnxj7eEkFRgl-McNXrGfvsOxyjE",
    authDomain: "wolvhavenpmstest.firebaseapp.com",
    databaseURL: "https://wolvhavenpmstest.firebaseio.com",
    projectId: "wolvhavenpmstest",
    storageBucket: "wolvhavenpmstest.appspot.com",
    messagingSenderId: "383468880334"
});

export const db = app.database();
export const refPlayers = app.database().ref('Players');
export const refWarnings = app.database().ref('Warnings');
export const refBans = app.database().ref('Bans');
export const refAccounts = app.database().ref('Accounts');
export const refUsers = app.database().ref('UserUUIDS');

export const auth = app.auth();
